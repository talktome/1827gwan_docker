# Docker image with G-WAN web server on Alpine Linux

Just the bare essentials (~185 MB).

Usage:

	docker build -t gwan https://bitbucket.org/talktome/1827gwan_docker.git
	docker run --rm gwan
