FROM alpine:3.7
ENV GLIBC_VERSION 2.27-r0
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub && \
	wget -q -O glibc.apk https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk && \
    wget -q -O glibc-bin.apk https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk && \
	wget -q -O gwan_linux64-bit.tar.bz2 http://gwan.com/archives/gwan_linux64-bit.tar.bz2 && \
    apk add --update glibc.apk glibc-bin.apk alpine-sdk && \
    tar -xjf gwan_linux64-bit.tar.bz2 && \
	rm -r /etc/apk/keys/sgerrand.rsa.pub glibc.apk glibc-bin.apk gwan_linux64-bit.tar.bz2 /var/*
CMD ["/gwan_linux64-bit/gwan"]
EXPOSE 8080 8081
